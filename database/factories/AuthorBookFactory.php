<?php

namespace Database\Factories;

use App\Models\Author;
use App\Models\AuthorBook;
use App\Models\Book;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\AuthorBook>
 */
class AuthorBookFactory extends Factory
{
    protected $model = AuthorBook::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $bookIds = \App\Models\Book::pluck('id')->toArray();
        $authorIds = \App\Models\Author::pluck('id')->toArray();

        return [
            'book_id' => $this->faker->randomElement($bookIds),
            'author_id' => $this->faker->randomElement($authorIds),
        ];
    }
}
