<?php

namespace Database\Factories;

use App\Models\Author;
use App\Models\Book;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Book>
 */
class BookFactory extends Factory
{

    protected $model = Book::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => $this->faker->sentence,
            'description' => $this->faker->paragraph,
            'image_path' => '',
            'published_at' => $this->faker->date,
        ];
    }

    public function configure()
    {
        return $this->afterCreating(function (Book $book) {
            // Убедитесь, что у каждой книги есть хотя бы один автор
            if ($book->authors->isEmpty()) {
                $author = Author::factory()->create();
                $book->authors()->attach($author);
            }
        });
    }
}
