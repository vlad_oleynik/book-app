<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return redirect()->route('books.index');
})->name('index');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::get('books', [\App\Http\Controllers\BookController::class , 'index'])->name('books.index');
Route::post('books', [\App\Http\Controllers\BookController::class , 'store'])->name('books.store');
Route::patch('books/{book}', [\App\Http\Controllers\BookController::class , 'update'])->name('books.update');
Route::delete('books/{book}', [\App\Http\Controllers\BookController::class , 'destroy'])->name('books.destroy');

Route::get('authors', [\App\Http\Controllers\AuthorController::class , 'index'])->name('authors.index');
Route::post('authors', [\App\Http\Controllers\AuthorController::class , 'store'])->name('authors.store');
Route::post('authors/search', [\App\Http\Controllers\AuthorController::class , 'search'])->name('authors.search');
Route::patch('authors/{author}', [\App\Http\Controllers\AuthorController::class , 'update'])->name('authors.update');
Route::delete('authors/{author}', [\App\Http\Controllers\AuthorController::class , 'destroy'])->name('authors.destroy');

require __DIR__.'/auth.php';
