<?php

namespace App\Services;

use App\Models\Author;
use Illuminate\Support\Carbon;

const DEFAULT_PAGINATION = 15;

class AuthorService
{
    public function search($request)
    {
        $authors = Author::query();

        if ($query = $request->search) {
            $authors = $authors->where('last_name', 'LIKE', "%$query%")
                ->orWhere('first_name', 'LIKE', "%$query%");
        }

        if ($sortById = $request->sortById) {
            $authors = $authors->orderBy('id', $sortById);
        }

        if ($sortByName = $request->sortByName) {
            $authors = $authors->orderBy('last_name', $sortByName);
        }

        return $authors->paginate(DEFAULT_PAGINATION);
    }
}
