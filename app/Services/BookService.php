<?php

namespace App\Services;

use App\Models\Book;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

const DEFAULT_PAGINATION = 15;

class BookService
{
    public function search($request)
    {
        $books = Book::query();

        if ($searchQuery = $request->input('search')) {
            $books->where(function ($query) use ($searchQuery) {
                $query->where('title', 'LIKE', "%$searchQuery%")
                    ->orWhere('description', 'LIKE', "%$searchQuery%")
                    ->orWhereHas('authors', function ($authorQuery) use ($searchQuery) {
                        $authorQuery->where('first_name', 'LIKE', "%$searchQuery%")
                            ->orWhere('last_name', 'LIKE', "%$searchQuery%");
                    });
            });
        }

        if ($sortByName = $request->sortByName) {
            $books = $books->orderBy('title', $sortByName);
        }

        if ($sortById = $request->sortById) {
            $books = $books->orderBy('id', $sortById);
        }

        return $books->paginate(DEFAULT_PAGINATION);
    }

    public function store($request)
    {
        $fileName = 'images/default-image.jpg';
        if (!is_null($request->file)){
            foreach ($request['file'] as $file) {
                $fileName = Storage::disk('public')->putFile('images', $file);
            }
        }

        $book = new Book($request->validated());
        $book->image_path = 'storage/' . $fileName;
        $book->save();
        $book->authors()->attach($request->input('authors'));

        return $book;
    }
}
