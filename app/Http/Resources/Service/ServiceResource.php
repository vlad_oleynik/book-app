<?php

namespace App\Http\Resources\Service;

use App\Http\Resources\Book\BookResource;
use App\Models\Salon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ServiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'price' => $this->price,
            'salon' => new BookResource(Salon::query()->findOrFail($this->salon_id)),
            'active' => $this->active,
            'date' => $this->updated_at->diffForHumans()
        ];
    }
}
