<?php

namespace App\Http\Controllers;

use App\Http\Requests\Author\StoreRequest;
use App\Http\Requests\Author\UpdateRequest;
use App\Http\Resources\Author\AuthorResource;
use App\Models\Author;
use App\Services\AuthorService;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    public AuthorService $authorService;

    public function __construct(AuthorService $authorService)
    {
        $this->authorService = $authorService;
    }

    public function index(Request $request)
    {
        $authors = $this->authorService->search($request);
        $authors = AuthorResource::collection($authors);

        return inertia('Author/Index' , compact('authors'));
    }

    public function store(StoreRequest $request)
    {
        Author::create($request->validated());
        return redirect()->route('authors.index');
    }

    public function update(UpdateRequest $request, Author $author)
    {
        $author->update($request->validated());
        return redirect()->route('authors.index');
    }

    public function destroy(Author $author)
    {
        $author->delete();
        return redirect()->route('authors.index');
    }
}
