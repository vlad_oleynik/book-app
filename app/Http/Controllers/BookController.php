<?php

namespace App\Http\Controllers;

use App\Http\Requests\Book\StoreRequest;
use App\Http\Requests\Book\UpdateRequest;
use App\Http\Resources\Author\AuthorResource;
use App\Http\Resources\Book\BookResource;
use App\Models\Author;
use App\Models\Book;
use App\Services\BookService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BookController extends Controller
{
    public BookService $bookService;

    public function __construct(BookService $bookService)
    {
        $this->bookService = $bookService;
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $books = $this->bookService->search($request);
        $authors = Author::all();
        $authors = AuthorResource::collection($authors)
            ->resolve();
        $books = BookResource::collection($books);

        return inertia('Index' , compact('books', 'authors'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request)
    {
        $this->bookService->store($request);

        return redirect()->route('index')->with('success', 'Book created successfully.');
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, Book $book)
    {
        $book->update($request->validated());
        $book->authors()->sync($request->input('authors'));

        return redirect()->route('books.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Book $book)
    {
        $book->authors()->detach();
        $book->delete();
        return redirect()->route('index')->with('success', 'Book deleted successfully.');;
    }
}
